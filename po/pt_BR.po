# Brazilian Portuguese translation of Monkey Bubble.
# Copyright (C) 2013 Free Software Foundation, Inc.
# Gustavo Maciel Dias Vieira <gdvieira@zaz.com.br>, 2004.
# Leonardo Gregianin <leogregianin@gmail.com>, 2008.
# Enrico Nicoletto <liverig@gmail.com>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: monkey-bubble 0.1.9\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=monkey-"
"bubble&keywords=I18N+L10N&component=general\n"
"POT-Creation-Date: 2012-08-11 09:58+0000\n"
"PO-Revision-Date: 2013-01-03 19:46-0300\n"
"Last-Translator: Enrico Nicoletto <liverig@gmail.com>\n"
"Language-Team: Brazilian Portuguese <gnome-pt_br-list@gnome.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: ../data/keybinding.ui.h:1
msgid "Keyboard Shortcuts"
msgstr "Atalhos de teclado"

#: ../data/keybinding.ui.h:2
msgid "_Shortcut Keys:"
msgstr "_Teclas de atalho:"

#: ../data/monkey-bubble.schemas.in.h:1
msgid "Left"
msgstr "Esquerda"

#: ../data/monkey-bubble.schemas.in.h:2
msgid "left key for player one"
msgstr "tecla esquerda para o jogador um"

#: ../data/monkey-bubble.schemas.in.h:3
msgid "Right"
msgstr "Direita"

#: ../data/monkey-bubble.schemas.in.h:4
msgid "right key for player one"
msgstr "tecla direita para o jogador um"

#: ../data/monkey-bubble.schemas.in.h:5
msgid "Up"
msgstr "Acima"

#: ../data/monkey-bubble.schemas.in.h:6
msgid "shoot key for player one"
msgstr "tecla de tiro para o jogador um"

#: ../data/monkey-bubble.schemas.in.h:7
msgid "s"
msgstr "s"

#: ../data/monkey-bubble.schemas.in.h:8
msgid "left key for player two"
msgstr "tecla esquerda para o jogador dois"

#: ../data/monkey-bubble.schemas.in.h:9
msgid "f"
msgstr "f"

#: ../data/monkey-bubble.schemas.in.h:10
msgid "right key for player two"
msgstr "tecla direita para o jogador dois"

#: ../data/monkey-bubble.schemas.in.h:11
msgid "d"
msgstr "d"

#: ../data/monkey-bubble.schemas.in.h:12
msgid "shoot key for player two"
msgstr "tecla de tiro para o jogador dois"

#: ../data/monkey-bubble.schemas.in.h:13
msgid "Pause"
msgstr "Pausar"

#: ../data/monkey-bubble.schemas.in.h:14
msgid "Pause the game"
msgstr "Pausar o jogo"

#: ../data/monkey-bubble.schemas.in.h:15
msgid "Stop the game"
msgstr "Parar o jogo"

#: ../data/monkey-bubble.schemas.in.h:16
msgid "Quit the game"
msgstr "Sair do jogo"

#: ../data/monkey-bubble.schemas.in.h:17
msgid "F1"
msgstr "F1"

#: ../data/monkey-bubble.schemas.in.h:18
msgid "1 player game"
msgstr "Jogo de 1 jogador"

#: ../data/monkey-bubble.schemas.in.h:19
msgid "F2"
msgstr "F2"

#: ../data/monkey-bubble.schemas.in.h:20
msgid "2 players game"
msgstr "Jogo de 2 jogadores"

#: ../data/monkey-bubble.ui.h:1
msgid "Warning"
msgstr "Aviso"

#: ../data/monkey-bubble.ui.h:2
msgid ""
"<markup>\n"
"<span weight=\"bold\" size=\"large\"\n"
"style=\"oblique\">\n"
"Can't create server !\n"
"</span>\n"
"</markup>"
msgstr ""
"<markup>\n"
"<span weight=\"bold\" size=\"large\"\n"
"style=\"oblique\">\n"
"Não foi possível criar o servidor !\n"
"</span>\n"
"</markup>"

#: ../data/netgame.ui.h:1 ../data/netserver.ui.h:1
#: ../src/ui/ui-network-client.c:173
msgid "Network game"
msgstr "Jogo em rede"

#: ../data/netgame.ui.h:2
msgid "_Server"
msgstr "_Servidor"

#: ../data/netgame.ui.h:3
msgid "_Go"
msgstr "_Ir"

#: ../data/netgame.ui.h:4
msgid "Number of players :"
msgstr "Número de jogadores:"

#: ../data/netgame.ui.h:5
msgid "Number of games :"
msgstr "Número de jogos:"

#: ../data/netgame.ui.h:6 ../data/netserver.ui.h:4
#: ../src/ui/keyboard-properties.c:85
msgid "Quit game"
msgstr "Sair do jogo"

#: ../data/netgame.ui.h:7
msgid "Close"
msgstr "Fechar"

#: ../data/netgame.ui.h:8 ../data/netserver.ui.h:5
msgid "Ready"
msgstr "Pronto"

#: ../data/netserver.ui.h:2
msgid "Number of game"
msgstr "Número do jogo:"

#: ../data/netserver.ui.h:3
msgid "Number of players"
msgstr "Número de jogadores:"

#: ../data/netserver.ui.h:6
msgid "Start Game"
msgstr "Iniciar jogo"

#: ../libgnomeui/gnome-scores.c:72
msgid "Top Ten"
msgstr "Top 10"

#: ../libgnomeui/gnome-scores.c:85
msgid "User"
msgstr "Usuário"

#: ../libgnomeui/gnome-scores.c:88
msgid "Score"
msgstr "Pontuação"

#: ../libgnomeui/gnome-scores.c:91
msgid "Date"
msgstr "Data"

# Ou seja:
# %a    is replaced by national representation of the abbreviated weekday name.
# %d    is replaced by the day of the month as a decimal number (01-31).
# %b    is replaced by national representation of the abbreviated month name.
# %Y    is replaced by the year with century as a decimal number.
# %T    is equivalent to ``%H:%M:%S''.
#. TRANSLATORS NOTE: Date format used when showing scores in games.
#. * Please refer to manpage of strftime(3) for complete reference.
#.
#: ../libgnomeui/gnome-scores.c:188
msgid "%a %b %d %T %Y"
msgstr "%a, %d de %b de %Y às %T"

#: ../libgnomeui/gnome-scores.c:389
msgid "Sans 14"
msgstr "Sans 14"

#: ../monkey-bubble.desktop.in.in.h:1 ../src/ui/main.c:97
#: ../src/ui/ui-main.c:259 ../src/ui/ui-main.c:271
msgid "Monkey Bubble"
msgstr "Monkey Bubble"

#: ../monkey-bubble.desktop.in.in.h:2
msgid "Monkey Bubble Arcade Game"
msgstr "Jogo Arcade Monkey Bubble"

#: ../src/ui/keyboard-properties.c:77
msgid "New game 1 player"
msgstr "Novo jogo de 1 jogador"

#: ../src/ui/keyboard-properties.c:79
msgid "New game 2 player"
msgstr "Novo jogo de 2 jogadores"

#: ../src/ui/keyboard-properties.c:81
msgid "Pause/Resume"
msgstr "Pausar/Continuar"

#: ../src/ui/keyboard-properties.c:83 ../src/ui/ui-main.c:225
msgid "Stop game"
msgstr "Parar jogo"

#: ../src/ui/keyboard-properties.c:91
msgid "Full Screen"
msgstr "Tela cheia"

#: ../src/ui/keyboard-properties.c:93
msgid "Normal Size"
msgstr "Tamanho normal"

#: ../src/ui/keyboard-properties.c:98 ../src/ui/keyboard-properties.c:108
msgid "aim left"
msgstr "mirar à esquerda"

#: ../src/ui/keyboard-properties.c:100 ../src/ui/keyboard-properties.c:110
msgid "aim right"
msgstr "mirar à direita"

#: ../src/ui/keyboard-properties.c:102 ../src/ui/keyboard-properties.c:112
msgid "shoot"
msgstr "atirar"

#: ../src/ui/keyboard-properties.c:119
msgid "Player 1"
msgstr "Jogador 1"

#: ../src/ui/keyboard-properties.c:120
msgid "Player 2"
msgstr "Jogador 2"

#: ../src/ui/keyboard-properties.c:121
msgid "Game"
msgstr "Jogo"

#: ../src/ui/keyboard-properties.c:122
msgid "View"
msgstr "Ver"

#: ../src/ui/keyboard-properties.c:307
#, c-format
msgid "There was an error loading config from %s. (%s)\n"
msgstr "Ocorreu um erro ao carregar configuração de %s. (%s)\n"

#: ../src/ui/keyboard-properties.c:321
#, c-format
msgid ""
"There was an error subscribing to notification of terminal keybinding "
"changes. (%s)\n"
msgstr ""
"Ocorreu um erro ao assinar a notificação de alterações de associações de "
"teclas do terminal. (%s)\n"

#: ../src/ui/keyboard-properties.c:364
#, c-format
msgid "There was an error loading a terminal keybinding. (%s)\n"
msgstr ""
"Ocorreu um erro ao carregar uma associação de teclas do terminal. (%s)\n"

#: ../src/ui/keyboard-properties.c:380
#, c-format
msgid "The value of configuration key %s is not valid; value is \"%s\"\n"
msgstr "O valor da chave de configuração %s não é válido; o valor é \"%s\"\n"

#: ../src/ui/keyboard-properties.c:403
#, c-format
msgid ""
"There was an error loading config value for whether to use menubar access "
"keys. (%s)\n"
msgstr ""
"Ocorreu um erro ao carregar o valor de configuração para uso de teclas de "
"acesso nos menus. (%s)\n"

#: ../src/ui/keyboard-properties.c:417
#, c-format
msgid ""
"There was an error subscribing to notification on changes on whether to use "
"menubar access keys (%s)\n"
msgstr ""
"Ocorreu um erro ao assinar a notificação de alterações de configuração sobre "
"o uso de teclas de acesso nos menus. (%s)\n"

#: ../src/ui/keyboard-properties.c:428
#, c-format
msgid ""
"There was an error loading config value for whether to use menu "
"accelerators. (%s)\n"
msgstr ""
"Ocorreu um erro ao carregar o valor de configuração para uso de aceleradores "
"de menus. (%s)\n"

#: ../src/ui/keyboard-properties.c:444
#, c-format
msgid ""
"There was an error subscribing to notification for use_menu_accelerators "
"(%s)\n"
msgstr ""
"Ocorreu um erro ao assinar a notificação para use_menu_accelerators (%s)\n"

#: ../src/ui/keyboard-properties.c:726
msgid "Disabled"
msgstr "Desabilitado"

#: ../src/ui/keyboard-properties.c:786
#, c-format
msgid "Error propagating accelerator change to configuration database: %s\n"
msgstr ""
"Erro ao propagar alteração de acelerador para o banco de dados de "
"configurações: %s\n"

#: ../src/ui/keyboard-properties.c:954
#, c-format
msgid "Error setting new accelerator in configuration database: %s\n"
msgstr ""
"Erro ao configurar novo acelerador no banco de dados de configurações: %s\n"

#: ../src/ui/keyboard-properties.c:1061
msgid "_Action"
msgstr "_Ação"

#: ../src/ui/keyboard-properties.c:1084
msgid "Shortcut _Key"
msgstr "_Tecla de atalho"

#: ../src/ui/main.c:87
msgid "Monkey Bubble is a Bust'a'Move clone for GNOME"
msgstr "Monkey Bubble é um clone do Bust'a'Move para o GNOME"

#: ../src/ui/ui-main.c:199
msgid "_Game"
msgstr "J_ogo"

#: ../src/ui/ui-main.c:200
msgid "New 1 player"
msgstr "Novo de 1 jogador"

#: ../src/ui/ui-main.c:203
msgid "Join _network game"
msgstr "Entrar em jogo em _rede"

#: ../src/ui/ui-main.c:206
msgid "Pause game"
msgstr "Pausar jogo"

#: ../src/ui/ui-main.c:209
msgid "Resume game"
msgstr "Continuar jogo"

#: ../src/ui/ui-main.c:213
msgid "New 2 players"
msgstr "Novo de 2 jogadores"

#: ../src/ui/ui-main.c:216
msgid "New network game"
msgstr "Novo jogo em rede"

#: ../src/ui/ui-main.c:222
msgid "_High Scores"
msgstr "_Pontuações máximas"

#: ../src/ui/ui-main.c:228
msgid "_Help"
msgstr "A_juda"

#: ../src/ui/ui-main.c:229
msgid "_Contents"
msgstr "Su_mário"

#: ../src/ui/ui-main.c:232
msgid "_About"
msgstr "_Sobre"

#: ../src/ui/ui-main.c:235
msgid "_Fullscreen"
msgstr "_Tela cheia"

#: ../src/ui/ui-main.c:238
msgid "_Leave Fullscreen"
msgstr "_Sair da tela cheia"

#: ../src/ui/ui-main.c:766
msgid ""
"Monkey Bubble is an Arcade Game for the GNOME Desktop Environment. Simply "
"remove all Bubbles by the creation of unicolor triplets."
msgstr ""
"Monkey Bubble é um jogo de arcade para o ambiente de trabalho GNOME. Basta "
"remover todas as Bolhas para criar trios de mesma cor."

#: ../src/ui/ui-main.c:773
msgid "translator_credits"
msgstr ""
"Gustavo Maciel Dias Vieira <gdvieira@zaz.com.br>\n"
"Leonardo Gregianin <leogregianin@gmail.com>"

#: ../src/ui/ui-main.c:794
#, c-format
msgid "There was an error displaying help: %s"
msgstr "Ocorreu um erro ao exibir a ajuda: %s"

#: ../src/ui/ui-network-client.c:209 ../src/ui/ui-network-server.c:168
msgid "_Player name"
msgstr "_Nome do jogador"

#: ../src/ui/ui-network-client.c:216 ../src/ui/ui-network-server.c:175
msgid "_Owner"
msgstr "_Proprietário"

#: ../src/ui/ui-network-client.c:223 ../src/ui/ui-network-server.c:182
msgid "_Ready"
msgstr "_Pronto"

#~ msgid "*"
#~ msgstr "*"

#~ msgid "Create network game"
#~ msgstr "Criar jogo em rede"

#~ msgid "_Settings"
#~ msgstr "_Configurações"

#~ msgid "Accelerator key"
#~ msgstr "Tecla aceleradora"

#~ msgid "Accelerator modifiers"
#~ msgstr "Modificadores do acelerador"

#~ msgid "Accelerator Mode"
#~ msgstr "Modo do acelerador"

#~ msgid "The type of accelerator."
#~ msgstr "O tipo do acelerador."

#~ msgid "Type a new accelerator, or press Backspace to clear"
#~ msgstr "Digite um novo acelerador ou pressione Backspace para limpar"

#~ msgid "Type a new accelerator"
#~ msgstr "Digite um novo acelerador"

#~ msgid "Players"
#~ msgstr "Jogadores"
